#!/bin/bash -
#
set -x
# BBCWW_wwsan02_ProgrammeMedia_Retention_Script v3.1
#
# Version History:
#
# 3.1 - Initial modification of preexisting v3 to prepare to modify log output location and tidy up commenting - BDC
# 3.4 - Running on site automatically at 5am
# 3.5 - JP - Updated the time retention to be 15 days as per the original v3 script running on Episode box
# 3.6 - JP - Updated the version number following a change to the retention days from 15 to 20 following request from Nicky in https://supportpartners.zendesk.com/agent/tickets/38591
# 3.7 - DM - Added .mxf files to the filter. Corrected name of 'folder list' .txt file following request 07043 https://supportpartners.my.salesforce.com/50058000006vImb
# 3.8 - JW - Changed retetion time to 15 days following request by A Dunne
# 3.9 - JW - Added .mp4 and .wav files to the filter following request by A Dunne
# 4.0 - JA - Extended file filter to all files following request from Asha
# 4.1 - JA - Amended for Aspera Folders

# Contributors:
# Original author - Daniele Mercanti
# Bill Cormack - BDC - bill.cormack@support-partners.com - +44 7860 719 771
#
# Variable Definitions:
#
# $FILELIST - Timestamped file listing files not accessed in more than 15 days
# $LOGFILE - Timestamped log file of the script progress
# $FOLDERLIST - Timestamped file listing the empty folders found
# $SOURCEDIR - The directory we are checking for old files and empty folders
# $DESTDIR - The quarantine folder we are moving the old files to

FILELIST="/Volumes/wwsan02/Deliveries/Shares/Clients/Edit Village/Documents/_Reversion Packages/Filelist/Filelist_$(date '+%Y-%m-%d %H-%M-%S')_reversionpackages_non_accessed_files.txt"
LOGFILE="/Volumes/wwsan02/Deliveries/Shares/Clients/Edit Village/Documents/_Reversion Packages/Logs/Logs_$(date '+%Y-%m-%d %H-%M-%S').log"
FOLDERLIST="/Volumes/wwsan02/Deliveries/Shares/Clients/Edit Village/Documents/_Reversion Packages/Folderlist/Folderlist_$(date '+%Y-%m-%d %H-%M-%S').txt"
#echo $FILELIST #Testing ONLY

if [ ! -e "$FILELIST" ]; then 	# If the $FILELIST file doesn't already exist then 
	
	touch "$FILELIST"	# Create the $FILELIST file
	
	if [ ! $? = 0 ]; then	# If file creation fails then 

		exit 1		# Log that there was a problem
		
	fi			# End failure if
	
fi				# End file doesn't exist if


#echo $LOGFILE #Testing ONLY

if [ ! -e "$LOGFILE" ]; then 	# If the $LOGFILE file doesn't already exist then 
	
	touch "$LOGFILE"	# Create the $LOGFILE file
	
	if [ ! $? = 0 ]; then	# If file creation fails then

		exit 2		# Log that there was a problem
		
	fi			# End creation failure if
	
fi				# End file doesn’t exist if

#echo $LOGFILE #Testing ONLY

if [ ! -e "$FOLDERLIST" ]; then	# If the $FOLDERLIST file doesn’t exist then 
	
	touch "$FOLDERLIST"		# Create the $FOLDERLIST file
	
	if [ ! $? = 0 ]; then		# If file creation fails

		exit 2			# Log that there was a problem
		
	fi				# End creation failure if
	
fi					# End file doesn’t exist if




SOURCEDIR="/Volumes/wwsan02/Departments/On Air/In Production/_Reversion Packages" # This is the directory to move files FROM
DESTDIR="/Volumes/wwsan02/Departments/On Air/In Production/_Reversion Packages/_Quarantine" # This is the directory to move files TO

find "$SOURCEDIR" ! -ipath "*_Quarantine*" -type f \( -iname "*.*" \) -atime +180d > "$FILELIST"	# Find all files more than 180 days (6 months) old or already in the quarantine folder and add them to the $FILELIST file

echo "======= Moving following files to _Quarantine folder =======" >> "$LOGFILE"
while read FILENAME	# While loop of each of the find results
do
	echo "$FILENAME" #TESTING ONLY
	
	if [ -e "$FILENAME" ]; then	# Check that the file still exists
		
# # #	echo "$FILENAME" >> "$LOGFILE" #TESTING ONLY
   	mv -v "$FILENAME" "$DESTDIR" >> "$LOGFILE" 2>&1	# Move the file to the $DESTDIR and append the results of the move to the $LOGFILE 
		
	fi	# End of the file exists if
	
done < "$FILELIST"	# End of the while loop

find /Volumes/wwsan02/Departments/On\ Air/In\ Production/_Reversion\ Packages ! -ipath "*_Quarantine*" -type d -mindepth 1 -maxdepth 2 -empty > "$FOLDERLIST"	# Find empty folders not in the Quarantine folder and append them to $FOLDERLIST

# # #	original simple shell script
echo "" >> "$LOGFILE"
echo "======= Moving following empty folders to _Empty_Folders =======" >> "$LOGFILE"
find /Volumes/wwsan02/Departments/On\ Air/In\ Production/_Reversion\ Packages ! -ipath "*_Quarantine*" -type d -mindepth 1 -maxdepth 2 -empty -exec mv '{}' /Volumes/wwsan02/Departments/On\ Air/In\ Production/_Reversion\ Packages/_Quarantine/_Empty_Folders \; >> "$LOGFILE" 2>&1	# Execute that same search and move the found folders to the quarantine folder and append the result of the move to the $LOGFILE


exit 0		# Exit without errors.
